# Maintainer: Oliver Smith <ollieparanoid@postmarketos.org>
# Co-Maintainer: Clayton Craft <clayton@craftyguy.net>
pkgname=postmarketos-mkinitfs
pkgver=1.0.2
pkgrel=1
pkgdesc="Tool to generate initramfs images for postmarketOS"
url="https://postmarketos.org"
depends="
	boot-deploy
	busybox-extras
	bzip2
	cryptsetup
	device-mapper
	e2fsprogs
	e2fsprogs-extra
	f2fs-tools
	lz4
	multipath-tools
	parted
	postmarketos-fde-unlocker
	xz
	"
makedepends="go"
replaces="mkinitfs"
triggers="$pkgname.trigger=/etc/postmarketos-mkinitfs/hooks:/usr/share/kernel/*:/usr/share/postmarketos-mkinitfs-triggers"
source="
	https://gitlab.com/postmarketOS/postmarketos-mkinitfs/-/archive/$pkgver/postmarketos-mkinitfs-$pkgver.tar.gz
	00-default.modules
	init.sh
	init_functions.sh
	"
install="$pkgname.post-upgrade"
arch="all"
license="GPL-2.0-or-later"
provides="mkinitfs=0.0.1"

export GOPATH="$srcdir"
export CGO_ENABLED=0

build() {
	# "-s -w" build a stripped binary
	go build -v -ldflags="-s -w"
}

package() {
	install -Dm644 "$srcdir/init_functions.sh" \
		"$pkgdir/usr/share/postmarketos-mkinitfs/init_functions.sh"

	install -Dm755 "$srcdir/init.sh" \
		"$pkgdir/usr/share/postmarketos-mkinitfs/init.sh"

	install -Dm644 "$srcdir/00-default.modules" \
		"$pkgdir/etc/postmarketos-mkinitfs/modules/00-default.modules"

	install -Dm755 postmarketos-mkinitfs \
		"$pkgdir/sbin/postmarketos-mkinitfs"

	ln -s /sbin/postmarketos-mkinitfs \
		"$pkgdir/sbin/mkinitfs"

	mkdir -p "$pkgdir/etc/postmarketos-mkinitfs/hooks/"
}

check() {
	go test ./...
}

sha512sums="
34ada6513a8048ffa25b2517661b42e14e61dd4565a671ef9a2c692ea7b65abf59299cd7640ad745974b97aaac0a4757b81da18ee5e97c10e9ad673ba0fb6ef4  postmarketos-mkinitfs-1.0.2.tar.gz
4717bf24bd13fd4d90f3ab000ab43f7d61529515de224ebc86458ae709ebe0b5065655457e34f5952126bac6ab45143a91cddb09a8ad2d0a55c5cecd564a0135  00-default.modules
40033b421e79999c85bd72e1353fe6745a87fcbf9f6a5b8180e832c7f340da7d4e33d056f557ae95a9924f5d186a6e728f3ed53c0922cdac4c39513fdc3e3a82  init.sh
563dfbd8e1ff1d3f42cd8f9615144fde83d0e1107fa1d6751eb7e049628cded12c0f1762d4ce498f741802b5f8449ef7cf1959fec22ea975fefeaa8050c171ec  init_functions.sh
"
